package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.HolyWish;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class HolyGrailTest {

    // TODO create tests
    public HolyGrail holyGrail;

    @BeforeEach
    public void setUp() throws Exception{
        holyGrail = new HolyGrail();
    }

    @Test
    public void testMakeAWish(){
        holyGrail.makeAWish("Wish me luck");
        assertEquals(holyGrail.getHolyWish().getWish(), "Wish me luck");
    }
}
