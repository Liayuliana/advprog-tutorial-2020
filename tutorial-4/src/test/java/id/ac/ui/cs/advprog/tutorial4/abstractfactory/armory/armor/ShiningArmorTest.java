package id.ac.ui.cs.advprog.tutorial4.abstractfactory.armory.armor;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.armor.Armor;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.armor.ShiningArmor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ShiningArmorTest {

    Armor shiningArmor;

    @BeforeEach
    public void setUp(){
        shiningArmor = new ShiningArmor();
    }

    @Test
    public void testToString(){
        // TODO create test
        assertEquals(shiningArmor.getName(), "Shining Armor");
    }

    @Test
    public void testDescription(){
        // TODO create test
        assertEquals(shiningArmor.getDescription(), "Armory jenis Shining Armor");
    }
}
