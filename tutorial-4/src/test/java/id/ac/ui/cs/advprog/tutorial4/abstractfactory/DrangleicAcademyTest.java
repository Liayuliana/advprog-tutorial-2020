package id.ac.ui.cs.advprog.tutorial4.abstractfactory;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.DrangleicAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.KnightAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MajesticKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.Knight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MetalClusterKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.SyntheticKnight;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.armor.MetalArmor;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.weapon.ShiningBuster;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.skill.ShiningForce;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class DrangleicAcademyTest {
    KnightAcademy drangleicAcademy;
    Knight majesticKnight;
    Knight metalClusterKnight;
    Knight syntheticKnight;

    @BeforeEach
    public void setUp() {
        // TODO setup me
        drangleicAcademy = new DrangleicAcademy();
        majesticKnight = drangleicAcademy.getKnight("majestic");
        metalClusterKnight = drangleicAcademy.getKnight("metal cluster");
        syntheticKnight = drangleicAcademy.getKnight("synthetic");
    }

    @Test
    public void checkKnightInstances() {
        // TODO create test
        assertTrue(majesticKnight instanceof MajesticKnight);
        assertTrue(metalClusterKnight instanceof MetalClusterKnight);
        assertTrue(syntheticKnight instanceof SyntheticKnight);
    }

    @Test
    public void checkKnightNames() {
        // TODO create test
        assertEquals(majesticKnight.getName(), "Majestic Knight");
        assertEquals(metalClusterKnight.getName(),"Metal Cluster Knight");
        assertEquals(syntheticKnight.getName(), "Synthetic Knight");
    }

    @Test
    public void checkKnightDescriptions() {
        // TODO create test
        assertEquals(majesticKnight.getArmor().getDescription(), "Armory jenis Metal Armor");
        assertEquals(majesticKnight.getWeapon().getDescription(), "Weaponary jenis Shining Buster");
        assertNull(majesticKnight.getSkill());

        assertEquals(metalClusterKnight.getArmor().getDescription(), "Armory jenis Metal Armor");
        assertEquals(metalClusterKnight.getSkill().getDescription(), "Skill jenis Shining Force");
        assertNull(metalClusterKnight.getWeapon());

        assertEquals(syntheticKnight.getWeapon().getDescription(), "Weaponary jenis Shining Buster");
        assertEquals(metalClusterKnight.getSkill().getDescription(), "Skill jenis Shining Force");
        assertNull(syntheticKnight.getArmor());
    }

}
